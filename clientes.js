const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const port = 8000;

app.get('/clientes', (req,res)=>{
    const source = req.query.nome;
    const ret = source;
    res.send('Nome encontrado = ' + ret);
})

app.post('/clientes', (req,res)=>{
    const dadosBody = req.body;
    const headers_ = req.headers['access'];
    let resp = '';
    console.log(dadosBody);
    if(headers_ == '123456'){
        resp = 'Acesso autorizado! Nome enviado: ' + dadosBody.nome;
    }else{
        resp = 'Acesso negado!';
    }
    res.send(`{Message: ${resp}}`);
})

app.listen(port,()=>{
    
})