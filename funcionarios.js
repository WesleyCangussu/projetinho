const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const port = 8080;

app.get('/funcionarios', (req,res)=>{
    const source = req.query.nome;
    const ret = source;
    res.send('Nome encontrado = ' + ret);
});

app.delete('/funcionarios/:nome',(req,res)=>{
    const source = req.params.nome;
    const headers_ = req.headers['access'];
    let resp = '';
    if(headers_ == '123456'){
        resp = 'Acesso autorizado! Nome deletado: ' + source;
    }else{
        resp = 'Acesso negado!';
    }
    res.send(`{Message: ${resp}}`);
});

app.put('/funcionarios', (req,res)=>{
    const dadosBody = req.body.nome;
    const headers_ = req.headers['access'];
    let ret = '';
    if(headers_ == '123456'){
        resp = 'Acesso autorizado! Nome enviado: ' + dadosBody;
    }else{
        resp = 'Acesso negado!';
    }
    res.send(`{Message: ${resp}}`);
});

app.listen(port,()=>{
    
})